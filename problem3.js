// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
function compare(a,b){
    if(a.car_model < b.car_model){
        return -1;
    }
    else{
        return 1;
    }
}
function problem3(inventory){
    if(!Array.isArray(inventory) || inventory.length===0 ){
        return [];
    }
    inventory.sort(compare);
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].car_model===undefined){
            return [];
        }
    }
    return inventory;
}
module.exports=problem3;