// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
function problem5(inventory,yearReq){
    if(!Array.isArray(inventory) || inventory.length===0 || typeof yearReq!== 'number'){
        return {};
    }
    const problem4=require('./problem4');
    const years=problem4(inventory);
    let olderYears=[];
    for(let i=0;i<years.length;i++){
        if(years[i]<yearReq){
            olderYears.push(years[i]);
        }
    }
    return olderYears;            
}
module.exports = problem5;