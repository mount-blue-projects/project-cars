// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
function problem4(inventory){
    if(!Array.isArray(inventory) || inventory.length===0){
        return [];
    }
    let arr=[];
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].car_year===undefined){
            return [];
        }
        arr.push(inventory[i].car_year);
    }
    return arr;
}
module.exports = problem4;